/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 13:43:57 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 20:54:12 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	first_elem_case(t_env *p)
{
	t_env	*tmp;

	free(p->name);
	free(p->val);
	tmp = p->next;
	free(p);
	p = env_first(tmp);
	return (0);
}

static void	other_elem_case(t_env *p, t_env *tmp)
{
	free(p->name);
	free(p->val);
	tmp->next = p->next;
	free(p);
}

int	ft_unset(int ac, char **av)
{
	t_env	*p;
	t_env	*tmp;

	while (--ac > 0)
	{
		p = env_first(NULL);
		if (!ft_strcmp(p->name, av[ac]))
			first_elem_case(p);
		else
		{
			while (p)
			{
				tmp = p;
				p = p->next;
				if (p && !ft_strcmp(p->name, av[ac]))
				{
					other_elem_case(p, tmp);
					break ;
				}
			}
		}
	}
	return (0);
}

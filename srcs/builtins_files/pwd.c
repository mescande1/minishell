/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/03 19:35:08 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/10 08:50:45 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_pwd(int ac, char **av)
{
	char	*pwd;

	(void)ac;
	(void)av;
	pwd = NULL;
	pwd = getcwd(pwd, 0);
	if (!pwd || printf("%s\n", pwd) < 0)
	{
		dprintf(2, "%s\n", strerror(errno));
		return (errno);
	}
	free(pwd);
	return (0);
}

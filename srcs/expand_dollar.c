/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_dollar.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/14 15:11:59 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 17:27:44 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
 * Find all environment variables in a string throught multiple calls
*/
static int	replace_it(char **str, int start, int len)
{
	char	*tmp;
	char	*res;
	int		ret;

	res = ft_strldup(*str + start + 1, len - 1);
	if (!res)
		return (-1);
	tmp = ft_getenv(res);
	free(res);
	ret = ft_strlen(tmp);
	res = malloc(ft_strlen(*str) - len + ret + 2);
	if (!res)
		return (-1);
	ft_memcpy(res, *str, start);
	ft_memcpy(res + start, tmp, ret);
	ft_memcpy(res + start + ret, *str + start + len,
		ft_strlen(*str + start + len));
	res[ft_strlen(*str) - len + ret] = 0;
	res[ft_strlen(*str) - len + ret + 1] = 0;
	free(*str);
	*str = res;
	return (ret);
}

int	mesure_dollar(char **str, int i)
{
	int			j;

	j = 1;
	if (ft_isdigit((*str)[i + j]))
		j++;
	else
		while (ft_is_envvar((*str)[i + j]))
			j++;
	if (j == 1 && (*str)[i + j] == '?')
		j++;
	j = replace_it(str, i, j);
	if (j == -1)
	{
		error_manager("dollar_interpretation failed");
		return (0);
	}
	return (j);
}

int	expand_dollar(char **str, int i, t_tree **root)
{
	const int	len = mesure_dollar(str, i);
	int			j;
	int			k;
	t_tree		*last;

	last = *root;
	j = 0;
	k = 0;
	while ((*str)[j + i] && j < len)
	{
		if (ft_isspace((*str)[i + j]))
		{
			if (k && insert_elem(&last, ft_strldup(*str + i + k, j - k)))
				return (errno);
			(*str)[i + j] = 0;
			while ((*str)[i + j + 1] && ft_isspace((*str)[i + j + 1]))
				j++;
			k = j + 1;
		}
		j++;
	}
	if (k && insert_elem(&last, ft_strdup(*str + i + k)))
		return (errno);
	return (j);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_manager_1.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 13:24:05 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/14 15:49:29 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	error_manager(char *str)
{
	error_handler(str);
	g_signal = errno;
	return (errno);
}

void	free_fd(t_fd *fd)
{
	t_fd	*old;

	old = NULL;
	while (fd)
	{
		old = fd;
		close(old->fd_[0]);
		if (old->input_fd)
			close(old->input_fd);
		if (old->output_fd)
			close(old->output_fd);
		fd = (fd)->next;
		free(old);
		old = NULL;
	}
}

void	free_table(char **table)
{
	int		i;

	i = 0;
	if (table)
	{
		while (*(table + i))
		{
			if (*(table + i))
				free(*(table + i));
			*(table + i) = NULL;
			i++;
		}
		free(table);
		table = NULL;
	}
}

void	free_ptr(void **ptr)
{
	free(*ptr);
	*ptr = NULL;
}

int	print_n_return(char *str, int val)
{
	dprintf(2, "\033[33m%s\n\r\033[0m", str);
	return (val);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcaps_arrows.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 19:52:39 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/07 20:11:03 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	update_line(t_hist *wo, int *buf_pos)
{
	t_buf	*buf;
	char	*tmp;

	*buf_pos = 0;
	buf = &wo->edit;
	if (!buf->str)
		if (buffer_append_str(buf, wo->line))
			return (error_manager("Line dupplication failed"));
	*buf_pos = buf->s - buf->str;
	if (user_prompt() == -1 || write(1, buf->str, *buf_pos) == -1)
		return (error_manager("Prompt error"));
	if (tputs(tgetstr("ce", NULL), 1, putint) == ERR)
		return (error_manager("tputs failed"));
	tmp = wo->edit.mov - 1;
	while (++tmp < wo->edit.s)
		if (tputs(tgetstr("le", NULL), 1, putint) == ERR)
			return (error_manager("tputs failed"));
	return (0);
}

int	history_up(t_hist **wo, int *buf_pos)
{
	if ((*wo)->prev)
	{
		*wo = (*wo)->prev;
		return (update_line(*wo, buf_pos));
	}
	return (0);
}

int	history_down(t_hist **wo, int *buf_pos)
{
	if ((*wo)->next)
	{
		*wo = (*wo)->next;
		return (update_line(*wo, buf_pos));
	}
	return (0);
}

int	move_left(t_hist **wo, int *buf_pos)
{
	(void)buf_pos;
	if ((*wo)->edit.mov > (*wo)->edit.str)
	{
		(*wo)->edit.mov--;
		if (tputs(tgetstr("le", NULL), 1, putint) == ERR)
			return (error_manager("tputs failed"));
	}
	return (0);
}

int	move_right(t_hist **wo, int *buf_pos)
{
	(void)buf_pos;
	if ((*wo)->edit.mov < (*wo)->edit.s)
	{
		(*wo)->edit.mov++;
		if (tputs(tgetstr("nd", NULL), 1, putint) == ERR)
			return (error_manager("tputs failed"));
	}
	return (0);
}

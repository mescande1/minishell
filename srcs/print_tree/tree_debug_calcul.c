/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree_debug_calcul.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 09:29:57 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/27 19:51:42 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	label_adjust(t_tree_display *me)
{
	me->label_delta = me->pos - me->labl_width / 2 - 1;
	if (me->label_delta < 0)
	{
		me->pos -= me->label_delta;
		me->label_delta = 0;
	}
}

static void	pos_calc(t_tree_display *me, t_tree_display *l, t_tree_display *r,
		int leaf[])
{
	const int	lpos = l->block_delta
		+ max(l->pos + 1, l->pos + (l->labl_width - me->labl_width) / 2);
	const int	rpos = l->block_width + l->block_delta + r->block_delta
		+ min(r->pos - 1, r->pos + (r->labl_width - me->labl_width) / 2);

	me->block_width = max(l->block_width + r->block_width + 1, me->labl_width);
	if (leaf[0] && leaf[1])
		me->pos = (lpos + rpos) / 2;
	else if (leaf[0])
	{
		me->pos = lpos;
		if (me->lwidth > me->labl_width)
			--me->block_width;
		me->rwidth = me->block_width - me->lwidth;
	}
	else if (leaf[1])
	{
		me->pos = rpos;
		if (me->rwidth > me->labl_width)
			--me->block_width;
		me->lwidth = me->block_width - me->rwidth;
	}
	else
		me->pos = me->block_width / 2 + 1;
}

static void	delta_adjust(t_tree *tree, t_tree_display *dsp[3], int leaf[2])
{
	const int	delta = dsp[0]->labl_width
		- (dsp[1]->block_width + dsp[2]->block_width + 1);

	if (delta > 0)
	{
		if (leaf[0] && leaf[1])
		{
			dsp[2]->block_delta = (delta + 1) / 2;
			tree->right->dsp.block_delta = dsp[2]->block_delta;
			dsp[1]->block_delta = delta - dsp[2]->block_delta;
			tree->left->dsp.block_delta = dsp[1]->block_delta;
		}
	}
	else if (leaf[0] && leaf[1])
		tree->right->dsp.block_delta = 1;
}

static void	init_calc(t_tree *tree, t_tree_display *me, t_tree_display *l,
		t_tree_display *r)
{
	bzero(l, sizeof(t_tree_display));
	bzero(r, sizeof(t_tree_display));
	me->depth = 1;
	me->labl_width = ft_strlen(tree->str);
	if (me->labl_width > MAX_WORD_WIDTH)
		me->trunc = 1;
	me->labl_width = min(me->labl_width, MAX_WORD_WIDTH) + WORD_ADDED_WIDTH;
}

int	_calc_depth(t_tree *tree, int depth, t_tree_display *me)
{
	t_tree_display	l;
	t_tree_display	r;
	int				leaf[2];

	if (!tree)
		return (0);
	init_calc(tree, me, &l, &r);
	leaf[0] = _calc_depth(tree->left, depth + 1, &l);
	leaf[1] = _calc_depth(tree->right, depth + 1, &r);
	delta_adjust(tree, (t_tree_display *[]){me, &l, &r}, leaf);
	me->lwidth = l.block_width;
	me->rwidth = r.block_width;
	pos_calc(me, &l, &r, leaf);
	label_adjust(me);
	tree->dsp = *me;
	return (max(depth, max(leaf[0], leaf[1])));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree_debug_buffer.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 09:28:30 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/07 20:18:34 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#define P_PAD " "
#define PAD " "

#define P_TRUNC "…"
#define TRUNC "+"

#define P_PREFIX "\033[33m☾\033[34m"
#define PREFIX "["

#define P_SUFFIX "\033[33m☽\033[0m"
#define SUFFIX "]"

#define P_JUNCTION "━"
#define JUNCTION "-"

#define P_JUNCTION_LOW_L "\033[35m╭"
#define JUNCTION_LOW_L "/"

#define P_JUNCTION_HIG_L "╯\033[0m"
#define JUNCTION_HIG_L "'"

#define P_JUNCTION_LOW_R "╮\033[0m"
#define JUNCTION_LOW_R "\\"

#define P_JUNCTION_HIG_R "\033[35m╰"
#define JUNCTION_HIG_R "`"

typedef struct s_print_style{
	char	*word;
	int		len;
}				t_print_style;

static const t_print_style	g_pretty_style[] = {
	[E_PAD] = {P_PAD, sizeof(P_PAD) - 1},
	[E_TRUNC] = {P_TRUNC, sizeof(P_TRUNC) - 1},
	[E_PREFIX] = {P_PREFIX, sizeof(P_PREFIX) - 1},
	[E_SUFFIX] = {P_SUFFIX, sizeof(P_SUFFIX) - 1},
	[E_JUNCTION] = {P_JUNCTION, sizeof(P_JUNCTION) - 1},
	[E_JUNCTION_LOW_L] = {P_JUNCTION_LOW_L, sizeof(P_JUNCTION_LOW_L) - 1},
	[E_JUNCTION_LOW_R] = {P_JUNCTION_LOW_R, sizeof(P_JUNCTION_LOW_R) - 1},
	[E_JUNCTION_HIG_L] = {P_JUNCTION_HIG_L, sizeof(P_JUNCTION_HIG_L) - 1},
	[E_JUNCTION_HIG_R] = {P_JUNCTION_HIG_R, sizeof(P_JUNCTION_HIG_R) - 1},
};

static const t_print_style	g_basic_style[] = {
	[E_PAD] = {PAD, 1},
	[E_TRUNC] = {TRUNC, 1},
	[E_PREFIX] = {PREFIX, 1},
	[E_SUFFIX] = {SUFFIX, 1},
	[E_JUNCTION] = {JUNCTION, 1},
	[E_JUNCTION_LOW_L] = {JUNCTION_LOW_L, 1},
	[E_JUNCTION_LOW_R] = {JUNCTION_LOW_R, 1},
	[E_JUNCTION_HIG_L] = {JUNCTION_HIG_L, 1},
	[E_JUNCTION_HIG_R] = {JUNCTION_HIG_R, 1},
};

int	buffer_append_raw(t_buf *b, const char *src, int len)
{
	const int	cur_len = b->s - b->str;
	const int	cur_pos = b->mov - b->str;

	if (cur_len + len + 1 >= b->capacity)
	{
		b->str = ft_memrealloc(b->str, b->capacity + 5048, b->capacity);
		b->capacity += 5048;
		if (!b->str)
		{
			b->s = NULL;
			b->capacity = 0;
			return (1);
		}
		b->s = b->str + cur_len;
		b->mov = b->str + cur_pos;
	}
	ft_memmove(b->mov + len, b->mov, b->s - b->mov);
	ft_memcpy(b->mov, src, len);
	b->s += len;
	b->mov += len;
	*b->s = 0;
	return (0);
}

int	buffer_append(t_buf *b, t_style_match item)
{
	if (b->pretty)
		return (buffer_append_raw(b, g_pretty_style[item].word,
				g_pretty_style[item].len));
	return (buffer_append_raw(b, g_basic_style[item].word,
			g_basic_style[item].len));
}

int	buffer_trim_to_newline(t_buf *b)
{
	while (b->s > b->str && *(b->s - 1) == ' ')
	{
		if (b->mov == b->s)
			b->mov--;
		b->s--;
	}
	return (buffer_append_raw(b, "\n", 1));
}

int	buffer_append_mul(t_buf *b, t_style_match item, int repeat)
{
	int		ret;

	if (repeat < 0)
		return (0);
	ret = 0;
	while (!ret && repeat--)
		ret = buffer_append(b, item);
	return (ret);
}

int	buffer_connect_leaf(t_buf *b, int is_l, int len, int *cumul)
{
	*cumul += len;
	if (is_l)
		return (buffer_append(b, E_JUNCTION_LOW_L)
			|| buffer_append_mul(b, E_JUNCTION, len - 2)
			|| buffer_append(b, E_JUNCTION_HIG_L)
		);
	return (buffer_append(b, E_JUNCTION_HIG_R)
		|| buffer_append_mul(b, E_JUNCTION, len - 2)
		|| buffer_append(b, E_JUNCTION_LOW_R));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree_debug_core.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 10:23:27 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/28 21:41:56 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	print_connect(t_tree *tree, t_buf *b, int *printed)
{
	int		print;
	int		ret;
	int		tmp;

	if (!tree->left)
		print = tree->dsp.pos - 1;
	else
		print = tree->left->dsp.pos + tree->left->dsp.block_delta - 1;
	ret = buffer_append_mul(b, E_PAD, print);
	if (ret || !tree->left)
		tmp = tree->dsp.lwidth;
	else
	{
		ret = buffer_connect_leaf(b, 1, tree->dsp.pos - print, &print);
		tmp = tree->left->dsp.block_width + tree->left->dsp.block_delta;
	}
	if (!ret && tree->right)
	{
		tmp = tree->right->dsp.pos + tree->right->dsp.block_delta + tmp
			- tree->dsp.pos + !tree->left;
		ret = buffer_connect_leaf(b, 0, tmp, &print);
	}
	*printed += print;
	tree->dsp.depth = 0;
	return (ret);
}

static int	print_label(t_tree *tree, t_buf *b, int *printed)
{
	const char	c = '0' + tree->separator;
	int			ret;

	ret = buffer_append_mul(b, E_PAD, tree->dsp.label_delta)
		|| buffer_append_raw(b, &c, 1)
		|| buffer_append(b, E_PREFIX)
		|| buffer_append_raw(b, tree->str,
			tree->dsp.labl_width - WORD_ADDED_WIDTH - tree->dsp.trunc);
	if (!ret && tree->dsp.trunc)
		ret = buffer_append(b, E_TRUNC);
	if (!ret)
		ret = buffer_append(b, E_SUFFIX);
	*printed += tree->dsp.labl_width + tree->dsp.label_delta;
	tree->dsp.depth = 2;
	return (ret);
}

static int	layer_print_specific(t_tree *tree, t_buf *b, int (*layer_fn)(),
		int *ret)
{
	int		print;

	if (*ret)
		return (0);
	print = tree->dsp.block_delta;
	*ret = buffer_append_mul(b, E_PAD, tree->dsp.block_delta);
	if (!*ret && tree->dsp.depth == 0)
	{
		if (tree->left)
			print += layer_print_specific(tree->left, b, layer_fn, ret);
		else
		{
			print += tree->dsp.lwidth;
			*ret = buffer_append_mul(b, E_PAD, tree->dsp.lwidth);
		}
		if (!*ret && tree->right)
			print += layer_print_specific(tree->right, b, layer_fn, ret);
	}
	else if (!*ret)
		*ret = layer_fn(tree, b, &print);
	if (!*ret)
		*ret = buffer_append_mul(b, E_PAD,
				tree->dsp.block_width + tree->dsp.block_delta - print);
	return (tree->dsp.block_width + tree->dsp.block_delta);
}

static int	layer_print_tree(t_tree *tree, int max_depth, int fd, int pretty)
{
	t_buf	b;
	int		ret;

	ret = 0;
	bzero(&b, sizeof(t_buf));
	b.pretty = pretty;
	while (!ret && max_depth >= 0)
	{
		layer_print_specific(tree, &b, print_label, &ret);
		if (!ret)
			ret = buffer_trim_to_newline(&b);
		if (max_depth--)
		{
			if (!ret)
				layer_print_specific(tree, &b, print_connect, &ret);
			if (!ret)
				ret = buffer_trim_to_newline(&b);
		}
	}
	if (!ret)
		ret = buffer_append_raw(&b, "\0", 1);
	if (!ret)
		dprintf(fd, "%s", b.str);
	free(b.str);
	return (ret);
}

int	_print_tree(t_tree *tree, int pretty, int fd)
{
	t_tree_display	first;
	int				ret;

	if (!tree)
	{
		if (pretty)
			dprintf(fd, "\033[31mCannot print a NULL tree\033[0m\n");
		else
			dprintf(fd, "Cannot print a NULL tree\n");
		return (0);
	}
	bzero(&first, sizeof(t_tree_display));
	ret = layer_print_tree(tree, _calc_depth(tree, 0, &first), fd, pretty);
	if (ret)
	{
		if (pretty)
			dprintf(fd, "\033[31mMemory error while printing tree\033[0m\n");
		else
			dprintf(fd, "Memory error while printing tree");
	}
	return (ret);
}

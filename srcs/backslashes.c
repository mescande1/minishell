/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backslashes.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/19 13:46:06 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 18:34:03 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	quotes_ignore(char **str, int i)
{
	int		j;

	j = 1;
	while ((*str)[j + i] && !ft_is_new_sep(*str, j + i, "\'\\"))
		j++;
	ft_memmove(*str + i + j, *str + i + j + 1, ft_strlen(*str + i + j));
	ft_memmove(*str + i, *str + i + 1, ft_strlen(*str + i));
	return (j - 1);
}

static int	find_backslashes_double_quotes(char **str, int i)
{
	if ((*str)[i + 1]
		&& ((*str)[i + 1] == '$' || (*str)[i + 1] == '\''
			|| (*str)[i + 1] == '\"'
			|| (*str)[i + 1] == '\\' || (*str)[i + 1] == '\n'))
		ft_memmove(*str + i, *str + i + 1, ft_strlen(*str + i) + 1);
	return (1);
}

int	double_quotes_ignore(char **str, int i)
{
	int	j;

	j = 1;
	while ((*str)[j + i] && (*str)[i + j] != '\"')
	{
		if ((*str)[j + i] == '$' && ft_is_envvar((*str)[i + j + 1]))
			j += mesure_dollar(str, i + j);
		else if ((*str)[j + i] == '\\')
			j += find_backslashes_double_quotes(str, i + j);
		else
			j++;
	}
	ft_memmove(*str + i + j, *str + i + j + 1, ft_strlen(*str + i + j));
	ft_memmove(*str + i, *str + i + 1, ft_strlen(*str + i));
	return (j - 1);
}

int	find_backslashes(char **str, int i)
{
	if ((*str)[i + 1])
		ft_memmove(*str + i, *str + i + 1, ft_strlen(*str + i) + 1);
	else
		(*str)[i] = ' ';
	return (1);
}

int	insert_elem(t_tree **last, char *str)
{
	const int	len = ft_strlen(str);
	t_tree		*elem;

	if (len == 0 || (len == 1 && str[0] == ' '))
		return (0);
	elem = ft_memalloc(sizeof(t_tree));
	if (!elem)
		return (error_manager("New leaf couldn't be create"));
	elem->str = str;
	elem->left = (*last)->left;
	(*last)->left = elem;
	(*last) = elem;
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_shell_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/11 15:01:59 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 15:47:17 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	user_prompt(void)
{
	if (write(1, "\r$", 2) == -1)
		return (-1);
	if (tputs(tgetstr("ce", NULL), 1, putint) == ERR)
		return (free_n_return("tputs failed", NULL, -1));
	return (1);
}

int	no_newline(t_hist *list)
{
	char	*pos;

	if (list->edit.str)
	{
		pos = ft_strchr(list->edit.str, '\n');
		if (pos)
		{
			ft_memmove(pos, pos + 1, max(ft_strlen(pos), 1));
			list->edit.s--;
			pos = ft_strchr(list->edit.str, '\n');
			if (pos)
			{
				g_signal = -3;
				return (print_n_return("Multi line not managed", 0));
			}
			return (0);
		}
	}
	return (1);
}

int	ft_miaou(t_hist **wo)
{
	char	buff[MAX_KEY_STRING_LENGTH];
	int		len;
	int		val;

	len = 0;
	while (no_newline(*wo) && g_signal == -1)
	{
		if (len && update_line((*wo), &len))
			return (errno);
		len = read(STDIN_FILENO, buff, MAX_KEY_STRING_LENGTH - 1);
		if (len < 0)
			return (error_manager("Standard input failed :"));
		if (len > 0)
		{
			buff[len] = 0;
			val = termcaps_handler(buff, wo);
			if (val)
				return (val);
		}
	}
	if (g_signal == -2 && (write(1, "^C", 2) == -1 || ft_setenv("?", "130")))
		return (error_manager(NULL));
	if (write(1, "\n\r", 2) == -1)
		return (error_manager(NULL));
	return (0);
}

int	new_shell_line(t_tree **root, t_hist **list)
{
	t_hist	*wo;
	char	*line;
	int		len;

	if (add_elem(list, NULL) || user_prompt() == -1)
		return (error_manager(NULL));
	wo = *list;
	line = NULL;
	len = ft_miaou(&wo);
	if (len)
		return (len);
	if (g_signal != -1)
		return (0);
	if (write_history(list, wo, &line))
		return (errno);
	if (line && tree_it(root, line))
		return (errno);
	if (write(1, "\r", 1) == -1)
		return (error_manager("Write failed"));
	return (0);
}

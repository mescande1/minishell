/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_env_var.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/04 22:20:21 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/14 15:58:02 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	set_shlvl(void)
{
	const char	*lvl = ft_getenv("SHLVL");
	char		*num;

	if (!lvl)
		ft_setenv("SHLVL", "0");
	else
	{
		num = ft_itoa(ft_atoi(lvl) + 1);
		ft_setenv("SHLVL", num);
		free(num);
	}
}

int	init_env_var(char **envp)
{
	t_env	*p;
	char	*val;

	p = new_env("?", "0", NULL);
	env_first(p);
	while (*envp)
	{
		val = *envp;
		while (*val && *val != '=')
			val++;
		if (!*val)
			continue ;
		*val = 0;
		val++;
		ft_setenv(*envp, val);
		envp++;
		val--;
		*val = 0;
	}
	set_shlvl();
	return (0);
}

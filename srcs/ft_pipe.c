/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipe.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 12:36:53 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 18:03:38 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	set_errstatus(t_fd *fd, int ret)
{
	int		wstatus;
	int		status;
	char	*code;

	if (!ret)
	{
		status = 127;
		while (fd)
		{
			status = fd->status;
			if (!fd->has_status)
			{
				waitpid(fd->cpid, &wstatus, WUNTRACED);
				if (WIFEXITED(wstatus))
					status = WEXITSTATUS(wstatus);
				else
					status = 128 + WTERMSIG(wstatus);
			}
			fd = fd->next;
		}
		ret = status;
	}
	code = ft_itoa(ret);
	ft_setenv("?", code);
	free(code);
}

int	ft_semicolon(t_tree *branch)
{
	int		skip;
	int		ret;
	t_fd	fd;

	ret = 0;
	fd.next = NULL;
	skip = 0;
	ret = fd_dealer(&fd, branch, 0);
	if (!ret)
		ret = pipe_handler(&fd, branch, &skip, 0);
	set_errstatus(fd.next, ret);
	free_fd(fd.next);
	return (ret);
}

int	ft_pipe(t_fd *fd, t_tree *branch, int piped)
{
	char		**argv;
	int			ret;

	spend_money(&branch);
	argv = exec_argv(branch);
	if (argv == NULL)
		return (1);
	ret = take_a_fork(fd, argv, piped);
	free(*argv);
	*argv = NULL;
	free(argv);
	argv = NULL;
	return (ret);
}

int	take_a_fork(t_fd *fd, char **argv, int piped)
{
	int			cpid;

	if (!piped && verif_builtins(fd, argv, 0))
		return (0);
	if (fd->input_fd)
		dup2(fd->input_fd, STDIN_FILENO);
	cpid = fork();
	if (cpid == -1)
		return (1);
	else if (cpid == 0)
		child_process(fd, argv);
	else if (cpid > 0)
		parent_process(cpid, fd);
	return (0);
}

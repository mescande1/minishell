/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_builtins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 14:37:49 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 14:00:50 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static const struct s_builtins	g_list[] = {
	{"echo", ft_echo},
	{"cd", ft_cd},
	{"pwd", ft_pwd},
	{"export", ft_export},
	{"unset", ft_unset},
	{"env", ft_env},
	{"exit", ft_exit},
	{NULL, 0}
};

static int	len_of(char **av)
{
	int	i;

	i = 0;
	while (av[i])
		i++;
	return (i);
}

int	filename_is_builtin(char *fn)
{
	int	i;

	i = -1;
	while (g_list[++i].name)
		if (!ft_strcmp(fn, g_list[i].name))
			return (1);
	return (0);
}

static void	*setupfd(int *backup_stdout, t_fd *fd_this)
{
	*backup_stdout = dup(STDOUT_FILENO);
	if (fd_this->input_fd)
		dup2(fd_this->input_fd, STDIN_FILENO);
	if (fd_this->output_fd)
		dup2(fd_this->output_fd, STDOUT_FILENO);
	else if (fd_this->next)
		dup2(fd_this->fd_[1], STDOUT_FILENO);
	return (fd_this);
}

static void	cleanfd(int backup_stdout, t_fd *fd_this)
{
	dup2(backup_stdout, STDOUT_FILENO);
	dup2(fd_this->fd_[0], STDIN_FILENO);
	close(fd_this->fd_[1]);
}

int	verif_builtins(t_fd *fd_this, char **av, int forked)
{
	int		i;
	int		backup_stdout;

	i = 0;
	while (g_list[i].name)
	{
		if (!ft_strcmp(av[0], g_list[i].name))
		{
			if (!forked)
				fd_this = setupfd(&backup_stdout, fd_this);
			fd_this->status = g_list[i].fct(len_of(av + 1), av + 1);
			fd_this->has_status = 1;
			if (!forked)
				cleanfd(backup_stdout, fd_this);
			return (1);
		}
		i++;
	}
	return (0);
}

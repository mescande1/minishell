/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 09:13:46 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/11 10:09:07 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
 * Manage when no history entry;
 * set line if needed, and set list 1 previous (because no history entry)
*/
int	no_save(t_hist **list, t_hist *wo, char **line)
{
	t_hist		*tmp;

	if (!*wo->edit.str)
		*line = NULL;
	else
		*line = (*list)->prev->line;
	free(wo->edit.str);
	tmp = (*list)->prev;
	if (tmp)
		tmp->next = NULL;
	ft_bzero(&(wo->edit), sizeof(t_buf));
	free((*list)->edit.str);
	ft_bzero(&(*list)->edit, sizeof(t_buf));
	free(*list);
	*list = tmp;
	return (0);
}

/*
 * trim string, to know if needed to be append
*/
static void	set_wo(t_hist *wo)
{
	wo->edit.mov = wo->edit.str;
	while (ft_isspace(*(wo->edit.mov)))
		wo->edit.mov++;
	while (wo->edit.s > wo->edit.mov && (ft_isspace(*(wo->edit.s - 1))))
		wo->edit.s--;
	*wo->edit.s = 0;
	ft_memmove(wo->edit.str, wo->edit.mov, wo->edit.s - wo->edit.mov + 1);
}

/*
 * Write just typed command in history if command is not just
 * spaces / command is empty;
 * save command in line, no free needed (freeed at free_history)
*/
int	write_history(t_hist **list, t_hist *wo, char **line)
{
	int		len;
	char	*name;
	int		fd;

	set_wo(wo);
	len = ft_strlen(wo->edit.str);
	if (!len || ((*list)->prev
			&& ft_strlen(wo->edit.str) == ft_strlen((*list)->prev->line)
			&& !ft_memcmp(wo->edit.str, (*list)->prev->line, len)))
		return (no_save(list, wo, line));
	(*list)->line = wo->edit.str;
	ft_bzero(&(wo->edit), sizeof(t_buf));
	free((*list)->edit.str);
	ft_memset(&(*list)->edit, 0, sizeof(t_buf));
	*line = (*list)->line;
	name = history_filename();
	if (!name)
		return (error_handler("Couldn't write history") * 0);
	fd = open(name, O_RDWR | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
	free(name);
	if (fd == -1)
		return (error_handler("Couldn't open history") * 0);
	if (write(fd, *line, len) == -1 || write(fd, "\n", 1) == -1)
		return (error_handler("Line couldn't be added to history") * 0);
	return (0);
}

/*
 * Created a new elem allocated on the heap, listing it in the list
*/
int	add_elem(t_hist **last, char *content)
{
	t_hist	*elem;

	elem = ft_memalloc(sizeof(t_tree));
	if (!elem)
		return (error_handler("Couldn't add_elem in local history"));
	elem->line = content;
	elem->prev = *last;
	if (last[0])
		last[0]->next = elem;
	*last = elem;
	return (0);
}

char	*history_filename(void)
{
	return (ft_strjoin(ft_getenv("HOME"), "/.minishell_history"));
}

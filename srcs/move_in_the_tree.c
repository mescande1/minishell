/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_in_the_tree.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/09 14:33:43 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 11:04:31 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_tree	*move_to_processing_file(t_tree *leaf)
{
	t_tree	*pos;

	pos = leaf;
	while (pos != NULL)
		if (pos->separator)
			pos = pos->left;
	return (pos);
}

int	move_in_the_tree(t_tree **root)
{
	int		backup_stdin;
	int		ret;

	backup_stdin = dup(STDIN_FILENO);
	ret = semi_handler(*root);
	dup2(backup_stdin, STDIN_FILENO);
	return (ret);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_var.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/04 21:19:17 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 16:19:10 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*env_first(t_env *elem)
{
	static t_env	*save;

	if (elem)
		save = elem;
	return (save);
}

char	*ft_getenv(const char *name)
{
	t_env	*p;

	p = env_first(NULL);
	while (p)
	{
		if (!ft_strcmp(p->name, name))
			return (p->val);
		p = p->next;
	}
	return (NULL);
}

t_env	*new_env(char *name, char *value, t_env *last)
{
	t_env	*elem;

	elem = ft_memalloc(sizeof(t_env));
	if (!elem)
	{
		error_handler("new env var not allocated");
		return (NULL);
	}
	elem->name = ft_strdup(name);
	elem->val = ft_strdup(value);
	if (last)
		last->next = elem;
	return (elem);
}

int	ft_setenv(char *name, char *value)
{
	t_env	*p;
	t_env	*tmp;

	p = env_first(NULL);
	if (ft_isdigit(*name))
		return (print_n_return("invalid identifier", 1));
	tmp = NULL;
	while (p)
	{
		if (!ft_strcmp(p->name, name))
		{
			free(p->val);
			p->val = ft_strdup(value);
			return (0);
		}
		tmp = p;
		p = p->next;
	}
	p = new_env(name, value, tmp);
	if (!p)
		return (ENOMEM);
	return (0);
}

int	ft_putenv(char *string)
{
	char	*s;

	s = string;
	while (ft_is_envvar(*s))
		s++;
	if (*s != '=')
		return (EINVAL);
	*s = 0;
	s++;
	return (ft_setenv(string, s));
}

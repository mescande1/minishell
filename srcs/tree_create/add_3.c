/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_3.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 15:45:52 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/11 16:25:24 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	cat_word(t_tree **leaf, char *str, size_t len)
{
	char	*tmp;

	tmp = ft_strnjoin((*leaf)->str, str, len);
	if (!tmp)
		return (error_manager("Cat word: strjoin failed"));
	(*leaf)->str = tmp;
	return (0);
}

int	cat_or_add(t_tree **leaf, char *str, size_t len)
{
	static int	new = 1;

	if (!leaf && !str)
		new = len;
	else if (new)
		return (add_word(leaf, str, len));
	else if (!new)
		return (cat_word(leaf, str, len));
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/22 16:56:36 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/14 16:39:18 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_tree	*ft_redir_handler(t_tree **leaf, t_tree **new)
{
	static t_tree	*leaf_save = NULL;
	static int		status = 0;

	if (new && new[0]->separator)
	{
		if (!ft_strchr("<>", new[0]->str[0]))
		{
			leaf_save = NULL;
			status = 0;
			return (new[0]);
		}
		if (!leaf_save)
			leaf_save = leaf[0];
		status = 1;
		return (new[0]);
	}
	if (status == 0 && leaf_save)
	{
		leaf[0] = leaf_save;
		leaf_save = NULL;
		return (leaf[0]);
	}
	status = 0;
	return (leaf[0]);
}

/*
 * Add a token word in the tree
*/
int	add_word(t_tree **leaf, char *str, size_t len)
{
	if (len == 0 || (len == 1 && str[0] == ' '))
		return (0);
	leaf[0] = ft_redir_handler(leaf, NULL);
	if (leaf[0]->str && leaf[0]->left)
	{
		leaf[0]->right = ft_memalloc(sizeof(t_tree));
		if (!leaf[0]->right)
			return (error_manager("New leaf couldn't be create"));
		leaf[0] = leaf[0]->right;
	}
	else if (leaf[0]->str)
	{
		leaf[0]->left = ft_memalloc(sizeof(t_tree));
		if (!leaf[0]->left)
			return (error_manager("New leaf couldn't be create"));
		leaf[0] = leaf[0]->left;
	}
	leaf[0]->str = ft_strldup(str, len);
	if (!leaf[0]->str)
		return (error_manager("strdup failed"));
	leaf[0]->len = len - 1;
	cat_or_add(NULL, NULL, 0);
	return (0);
}

/*
 * Put a Token sep in the Tree
 * Specific behavior with ';'
*/
static int	put_sep_in_tree(t_tree **leaf, t_tree **root, t_tree **new)
{
	if (root[0]->separator && !ft_strcmp(root[0]->str, ";")
		&& ft_strcmp(new[0]->str, ";"))
	{
		new[0]->left = root[0]->right;
		root[0]->right = new[0];
		leaf[0] = new[0];
	}
	else
	{
		new[0]->left = root[0];
		root[0] = new[0];
		leaf[0] = ft_redir_handler(leaf, new);
	}
	if (!ft_strcmp((*new)->str, ";"))
		reinitialised_redir_handler(*leaf);
	return (0);
}

/*
 * Add a token sep in the tree
*/
int	add_sep(t_tree **leaf, t_tree **root, char *sep)
{
	t_tree	*new;
	int		pass;

	pass = sep[0] == '>' || sep[0] == '<';
	if (!pass && !(*root)->str)
	{
		g_signal = -2;
		return (free_n_return(NULL, sep, 1));
	}
	new = ft_memalloc(sizeof(t_tree));
	if (!new)
		return (error_manager("New root couldn't be create"));
	new->left = root[0];
	new->str = sep;
	new->separator = 1 << (sep[0] == ';');
	if (!new->str)
		return (error_manager("strdup failed"));
	new->len = ft_strlen(new->str);
	if (put_sep_in_tree(leaf, root, &new))
	{
		g_signal = -2;
		return (1);
	}
	return (0);
}

int	add_sep_1(t_tree **leaf, t_tree **root, char **str, int j)
{
	const char	*tmp = ft_strldup(*str + j, 1);

	if (!tmp)
		return (error_manager("strdup failed"));
	*str += j + 1;
	cat_or_add(NULL, NULL, 1);
	return (add_sep(leaf, root, (char *)tmp));
}

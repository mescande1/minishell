/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dollar.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 14:11:01 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 18:48:11 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
 * Environment variables can contain alphanumeric characters or underscore
*/
int	ft_is_envvar(int c)
{
	return (ft_isalnum(c) || c == '_');
}

static int	tild_interpret(char **str, int i)
{
	const char	*home = ft_getenv("HOME");
	char		*tmp;

	tmp = ft_strnew(ft_strlen(*str) + ft_strlen(home));
	if (!tmp)
		return (error_manager("tild_interpret failed"));
	ft_strncpy(tmp, *str, i);
	ft_strcat(tmp + i, home);
	ft_strcat(tmp + i + ft_strlen(home), *str + i + 1);
	free(*str);
	*str = tmp;
	return (ft_strlen(home));
}

static int	interpret_line(char **str, t_tree **root)
{
	size_t	i;

	i = 0;
	while (g_signal == -1 && *str && i < ft_strlen(*str))
	{
		if ((*str)[i] == '\\')
			i += find_backslashes(str, i);
		else if ((*str)[i] == '\'')
			i += quotes_ignore(str, i);
		else if ((*str)[i] == '\"')
			i += double_quotes_ignore(str, i);
		else if ((*str)[i] == '$' && (ft_is_envvar((*str)[i + 1])
				|| (*str)[i + 1] == '?' || (*str)[i + 1] == '\"'))
			i += expand_dollar(str, i, root);
		else if ((*str)[i] == '~' && i == 0)
			i += tild_interpret(str, i);
		else
			i++;
	}
	if (g_signal != -1)
		return (g_signal);
	return (0);
}

/*
 * Interpret and replace environment variables
*/
int	spend_money(t_tree **root)
{
	int		res;

	res = 0;
	if ((*root)->left)
		res = spend_money(&((*root)->left));
	if (!res && (*root)->right)
		res = spend_money(&((*root)->right));
	if (!res)
	{
		ft_is_new_sep("bouh", 0, "'");
		interpret_line(&((*root)->str), root);
		if (!(*root)->str)
			return (error_manager("Environment variable interpretation fail"));
	}
	return (res);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/20 14:57:26 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 16:08:19 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# define _GNU_SOURCE
# define _XOPEN_SOURCE
# define _XOPEN_SOURCE_EXTENDED

# include "libft.h"
# include "get_next_line.h"
# include "tree.h"
# include "tree_debug.h"
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/resource.h>
# include <sys/stat.h>
# include <sys/sysmacros.h>
# include <signal.h>
# include <dirent.h>
# include <string.h>
# include <errno.h>
# include <sys/ioctl.h>
# include <termios.h>
# include <ncursesw/curses.h>
# include <term.h>
# include <stddef.h>

# define MAX_KEY_STRING_LENGTH 2048

typedef struct s_tree_display	t_tree_display;

typedef struct s_binary_tree_elem {
	char						*str;
	int							len;
	char						separator;
	t_tree_display				dsp;
	struct s_binary_tree_elem	*left;
	struct s_binary_tree_elem	*right;
}	t_tree;

struct s_parse_assign_function {
	char				*id;
	char				len;
	int					(*fct)(t_tree **leaf, t_tree **root, char **str, int j);
};

typedef struct s_history_list {
	char						*line;
	t_buf						edit;
	struct s_history_list		*next;
	struct s_history_list		*prev;
}	t_hist;

struct s_termcaps {
	char						*name;
	char						*value;
	int							len;
	int							(*fct)(t_hist **wo, int *buf_pos);
};

struct s_builtins {
	char						*name;
	int							(*fct)(int ac, char **av);
};

typedef struct s_global_process_status {
	char						value;
}								t_signal;

extern int						g_signal;

typedef struct s_fd {
	int							fd_[2];
	int							input_fd;
	int							output_fd;
	int							cpid;
	int							status;
	int							has_status;
	struct s_fd					*next;
}								t_fd;

typedef struct s_env_var_list {
	char						*name;
	char						*val;
	struct s_env_var_list		*next;
}								t_env;

/*
**	Init
*/
void	default_terminal_mode(struct termios *tattr);
void	raw_terminal_mode(struct termios *tattr_save);
int		init_main(t_hist **list, char **envp);
int		init_parse(t_tree *t);
int		init_env_var(char **envp);

/*
 *	Signals
*/
void	ctrl_c(int val);

/*
**	Parsing step
*/
int		user_prompt(void);
int		new_shell_line(t_tree **root, t_hist **list);

/*
 * History management
*/
int		write_history(t_hist **list, t_hist *wo, char **line);
int		add_elem(t_hist **last, char *content);
char	*history_filename(void);

/*
 * Termcaps
*/
int		update_line(t_hist *wo, int *buf_pos);
int		putint(int c);
int		history_up(t_hist **wo, int *buf_pos);
int		history_down(t_hist **wo, int *buf_pos);
int		move_left(t_hist **wo, int *buf_pos);
int		ctrl_left(t_hist **wo, int *buf_pos);
int		move_right(t_hist **wo, int *buf_pos);
int		ctrl_right(t_hist **wo, int *buf_pos);
int		end(t_hist **wo, int *buf_pos);
int		home(t_hist **wo, int *buf_pos);
int		del(t_hist **wo, int *buf_pos);
int		backspace(t_hist **wo, int *buf_pos);

int		termcaps_handler(char *buff, t_hist **wo);

/*
 * Line interpretation ($ and \)
*/
int		find_backslashes(char **str, int i);
int		ft_is_envvar(int c);
int		mesure_dollar(char **str, int i);
int		expand_dollar(char **str, int i, t_tree **root);
int		double_quotes_ignore(char **str, int i);
int		quotes_ignore(char **str, int i);
int		spend_money(t_tree **root);

/*
 * Environment variables management
*/
t_env	*env_first(t_env *elem);
char	*ft_getenv(const char *name);
t_env	*new_env(char *name, char *value, t_env *last);
int		ft_setenv(char *name, char *value);
int		ft_putenv(char *string);

/*
**	Tree management
*/
int		insert_elem(t_tree **last, char *str);
int		ft_is_new_sep(char *str, int j, char *seps);
int		test_op(t_tree **root, int semi);
int		tree_it(t_tree **root, char *line);

int		add_word(t_tree **leaf, char *str, size_t len);
int		cat_word(t_tree **leaf, char *str, size_t len);
int		cat_or_add(t_tree **leaf, char *str, size_t len);

t_tree	*ft_redir_handler(t_tree **leaf, t_tree **new_elem);
int		add_sep(t_tree **leaf, t_tree **root, char *sep);
int		add_sep_1(t_tree **leaf, t_tree **root, char **str, int j);
int		add_sep_2(t_tree **leaf, t_tree **root, char **str, int j);
int		add_quote(t_tree **leaf, t_tree **root, char **str, int j);
int		add_double_quote(t_tree **leaf, t_tree **root, char **str, int j);
int		eol(t_tree **leaf, t_tree **root, char **str, int j);
int		space(t_tree **leaf, t_tree **root, char **str, int j);

int		reinitialised_redir_handler(t_tree *leaf);

/*
**	Print Tree
*/
int		min(int a, int b);
int		max(int a, int b);
void	print_t(t_tree *tree);

/*
**	execution part
*/
int		move_in_the_tree(t_tree **root);
t_tree	*move_to_processing_file(t_tree *leaf);
int		semi_handler(t_tree *branch);
int		pipe_handler(t_fd *fd, t_tree *branch, int *skip, int piped);
int		output_handler(t_fd **fd, t_tree *branch);
char	*sepchar(int (*ft_separator)(t_fd **fd, t_tree *branch));
int		ft_semicolon(t_tree *branch);
int		ft_pipe(t_fd *fd, t_tree *branch, int piped);
int		fd_input(t_fd *fd, t_tree *branch);
int		fd_output(t_fd *fd, t_tree *branch);
int		fd_output_append(t_fd *fd, t_tree *branch);
int		input_handler(t_fd **fd, t_tree *branch);
int		ft_stdinfd_to_fd(t_fd **fd, t_tree *branch);
int		through_pipe(t_fd *fd, t_tree *branch);
char	**exec_argv(t_tree *branch);
int		take_a_fork(t_fd *fd, char **argv, int piped);
void	child_process(t_fd *fd, char **argv);
void	parent_process(int cpid, t_fd *fd);
int		fd_dealer(t_fd *fd, t_tree *branch, int already_created);
int		count_pipe(t_tree *branch);

/*
 * builtins
*/
int		ft_cd(int ac, char **av);
int		ft_env(int ac, char **av);
int		ft_exit(int ac, char **av);
int		ft_export(int ac, char **av);
int		ft_pwd(int ac, char **av);
int		ft_unset(int ac, char **av);
int		ft_echo(int ac, char **av);

char	*ft_strjoin_nfree(char *s1, const char *s2);
int		filename_is_builtin(char *fn);
int		verif_builtins(t_fd *fd, char **av, int forked);

/*
**	Error management
*/
void	*free_tree(t_tree **root);
int		free_history(t_hist **elem, int val);
void	free_env(void);
int		free_n_return(char *str, void *to_free, int val);
int		error_manager(char *s);

int		error_handler(char *s);
void	free_fd(t_fd *fd);
void	free_table(char **table);
void	free_ptr(void **ptr);
int		print_n_return(char *str, int val);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree_debug.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 09:25:56 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/28 21:05:31 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TREE_DEBUG_H
# define TREE_DEBUG_H

# include "tree.h"

typedef enum e_style_match {
	E_PAD,
	E_TRUNC,
	E_PREFIX,
	E_SUFFIX,
	E_JUNCTION,
	E_JUNCTION_LOW_L,
	E_JUNCTION_LOW_R,
	E_JUNCTION_HIG_L,
	E_JUNCTION_HIG_R,
}	t_style_match;

typedef struct s_buf {
	char	*str;
	char	*s;
	char	*mov;
	int		capacity;
	int		pretty;
}	t_buf;

int		buffer_append_raw(t_buf *b, const char *src, int len);
int		buffer_append_str(t_buf *b, char *str);
int		buffer_append(t_buf *b, t_style_match item);
int		buffer_trim_to_newline(t_buf *b);
int		buffer_append_mul(t_buf *b, t_style_match item, int repeat);
int		buffer_connect_leaf(t_buf *b, int is_l, int len, int *cumul);

int		_calc_depth(t_tree *tree, int depth, t_tree_display *me);
int		_print_tree(t_tree *tree, int pretty, int fd);

#endif
